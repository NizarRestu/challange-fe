import { BrowserRouter, Route, Switch } from "react-router-dom";
import Register  from "./Register";
import Login from "./css/Login";
import Home from "./Home";
function App() {
  return (
    <div className="App">
    <BrowserRouter>
    <main>
      <Switch>
      <Route path="/reg" component={Register} exact />
      <Route path="/" component={Login} exact />
      <Route path="/home" component={Home} exact />
      </Switch>
    </main>
    </BrowserRouter>
    </div>
  );
}

export default App;
