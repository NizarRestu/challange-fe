import axios from "axios";
import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useHistory } from "react-router-dom";
import "./css/Register.css";
const Register = () => {
  const history = useHistory();
  const [nama, setNama] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");

  const register = async (e) => {
    e.preventDefault();

    const req = {
      username: nama,
      password: password,
      email: email,
    };

    try {
      await axios
        .post("http://localhost:5000/register/reg", req)
        .then(() => {
          alert("success");
        })
        .catch((error) => {
          alert(error);
        });
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div className="induk">
      <div className="box">
        <h1>Form Register</h1>
        <Form onSubmit={register} className="box2">
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Username</Form.Label>
            <Form.Control
              onChange={(e) => setNama(e.target.value)}
              required
              type="text"
              placeholder="Enter Username"
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control
              required
              onChange={(e) => setEmail(e.target.value)}
              type="email"
              placeholder="Enter email"
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              required
              onChange={(e) => setPassword(e.target.value)}
              type="password"
              placeholder="Password"
            />
          </Form.Group>
          <div className="btn-box">
            <Button variant="primary" type="submit">
              Register
            </Button>
            <p className="text">
              Jika sudah punya akun silahkan <a href="/">login</a>
            </p>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Register;
