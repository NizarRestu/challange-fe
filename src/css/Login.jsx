import axios from "axios";
import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useHistory } from "react-router-dom";

const Login = () => {
    const histroy = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const login = async (e) => {
    e.preventDefault();
    try {
      const { data, status } = await axios.post(
        "http://localhost:5000/register/sign-in",
        {
          email: email,
          password: password,
        }
      );
      //Jika response  status 200 ok
      if (status === 200) {
       alert("succses")
       histroy.push("/home")
      }
    } catch (error) {
      alert(error)
    }
  };
  return (
    <div className="induk">
      <div className="box">
        <h1>Form Login</h1>
        <Form onSubmit={login} className="box2">
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control
              required
              onChange={(e) => setEmail(e.target.value)}
              type="email"
              placeholder="Enter email"
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              required
              onChange={(e) => setPassword(e.target.value)}
              type="password"
              placeholder="Password"
            />
          </Form.Group>
          <div className="btn-box">
            <Button variant="primary" type="submit">
              Login
            </Button>
            <p className="text">
              Jika belum punya akun silahkan <a href="">register</a>
            </p>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Login;
